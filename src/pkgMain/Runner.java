package pkgMain;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.imageio.ImageIO;

import org.ini4j.Wini;



public class Runner {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		
		
		System.out.println("start");

		//InputGenerator inputGenerator = new InputGenerator();
		//inputGenerator.generateCSVFromImages();

		//inputGenerator.NormalizeData("data/train.csv","data/train_norm.csv",400);
		//inputGenerator.NormalizeData("data/validation.csv","data/validation_norm.csv",400);
		
		Wini ini = new Wini(new File("settings.ini"));
		String nnFile = ini.get("data", "nn_file");
		int chunkSize = ini.get("data", "chunkSize", int.class);
		int restThreshold = ini.get("data", "restThreshold", int.class);
		int restSeconds = ini.get("data", "restSeconds", int.class);
		int imageCountThreshold = ini.get("data", "imageCountThreshold", int.class);

		
		
		/*
		TextFinder textFinder = new TextFinder();
		textFinder.loadNetwork("d:/data/best.net");
		textFinder.loadImage("d:/data/photos/1.jpg");
		textFinder.generateNormilizedMatrix();
		textFinder.markText(.9999);
		/**/
		
		
		
		TextFinderNN textFinderNN = new TextFinderNN(); 
		textFinderNN.loadNetwork(nnFile);
		
		
		/*textFinderNN.loadImage("d:/data/photos/3.jpg");
	    System.out.println("found text: "+textFinderNN.findText(.9999));*/
	    
		
		CarList carList = new CarList(imageCountThreshold);
		
		/**/
		
		TimeLogger tlFetchList = new TimeLogger("Fetch List");
		TimeLogger tlFindImages = new TimeLogger("Find images");

		while (keepServiceAlive()) {
			
			tlFetchList.startTimer("Fetch List");
			carList.fetchList(chunkSize);
			
			System.out.println("CarList.cars.size() = "+carList.cars.size());
			if (carList.cars.size()<= restThreshold ) {
				try {
				    Thread.sleep(60*1000);                 //1000 milliseconds is one second.
				} catch(InterruptedException ex) {
				    Thread.currentThread().interrupt();
				}
			}
			
			tlFetchList.stopTimer();
			
			tlFindImages.startTimer("Find images");
			carList.findTextInImages(textFinderNN);
			tlFindImages.stopTimer();
			
			carList.updateLastCarId();
			System.out.println("maxCarId = "+carList.maxCarId);
			
			
		}
		
		
		
		System.out.println("done");
	}
	
	public static boolean keepServiceAlive(){
		Connection conn = DB.connectToDB("myauto");
		
		int iKeepAlive=0;
		
		String query = "select meta_data_value_int from meta_data where meta_data_id = 2";
		  
		Statement st;
		try {
			st = conn.createStatement();
			ResultSet rs = st.executeQuery(query);

			rs.next();
			iKeepAlive = rs.getInt("meta_data_value_int");
			
			st.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}		
		
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (iKeepAlive==1) return true; else return false; 
	}

}
