package pkgMain;


import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

import org.encog.ml.data.MLData;
import org.encog.ml.data.basic.BasicMLData;
import org.encog.neural.networks.BasicNetwork;
import org.encog.util.obj.SerializeObject;

public class TextFinderNN {
	BasicNetwork network;
	double[][] normalizedMatrix;
	
	int frameWidth = 20;
	int frameHeight = 20;
	BasicMLData input = new BasicMLData(frameWidth * frameHeight);
	

	public void loadNetwork(String fileName) {
		try {
			network = (BasicNetwork) SerializeObject.load(new File(fileName));
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
	}
	
	public void loadImage(String p_url, int p_url_type) {
		BufferedImage bImage = null;
		try {
			if (p_url_type==1) bImage = ImageIO.read(new URL(p_url));// url 
			if (p_url_type==2) bImage = ImageIO.read(new File(p_url)); // filename
			
			normalizedMatrix = generateNormalizedMatrix(bImage);
			
		} catch (IOException e) {
			e.printStackTrace();
		}

	}	
	
	public void copyFrameToInput(int x, int y) {
		input.clear();
		for (int i = 0; i < frameWidth * frameHeight; i++)
			input.add(i, normalizedMatrix[x + (i % frameWidth)][y + (i / frameWidth)]);
	}	
	
	public int findText(double threshold) {
		//TimeLogger tlFindText = new TimeLogger("Find Text");
		int textCount = 0;
		MLData output;
		for (int x = 0; x < normalizedMatrix.length - frameWidth; x += 10) {
			for (int y = 0; y < normalizedMatrix[0].length - frameHeight; y += 10) {
				copyFrameToInput(x, y);
				output = network.compute(input);
				//System.out.println("x=" + x + ", y=" + y + "; " + String.format("%.5f", output.getData()[0]));
				// showInput();
				if (output.getData()[0] > threshold) textCount++; 
			}
		}	
		//tlFindText.stopTimer();
		return textCount;
	}
	
	public static double[][] generateNormalizedMatrix(BufferedImage bImage){
		
		double[][] normalizedMatrix = new double[bImage.getWidth()][bImage.getHeight()];
		int p = 0, r = 0, g = 0, b = 0, newPixel=0;
		
		for (int j = 0; j < bImage.getHeight(); j++) {
			for (int i = 0; i < bImage.getWidth(); i++) {
				
				p = bImage.getRGB(i,j);
				r = (p>>16)&0xff;
				g = (p>>8)&0xff;
				b = p&0xff;

				newPixel = (r + g + b) / 3;
				normalizedMatrix[i][j] = (255 - newPixel) / 255.0;

			}
	    } 		
		
		return normalizedMatrix;
	}

}
