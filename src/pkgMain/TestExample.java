package pkgMain;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.encog.Encog;

import org.encog.engine.network.activation.ActivationTANH;
import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataPair;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLData;
import org.encog.ml.data.basic.BasicMLDataSet;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;
import org.encog.neural.networks.training.Train;
import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation;
import org.encog.util.obj.SerializeObject;

public class TestExample {
	int inputSize = 28*28;
	int outputSize = 10;
	
	double old_error = 10000;

	public BasicMLData data_input = new BasicMLData(inputSize);
	public BasicMLData data_ideal = new BasicMLData(1);

	
	
	public void run() throws IOException {

		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		
		int totalCount = 0;
		int correctCount = 0;

		MLDataSet trainingSet = new BasicMLDataSet();
		//MLDataSet posTrainingSet = new BasicMLDataSet();
		
        String trainFile = "d:/data/mnist/mnist_train.csv";
		//String trainFile = "data/mnist/xor.csv";
        //String testFile = "data/mnist/mnist_test.csv";
        
      
       
		br = new BufferedReader(new FileReader(trainFile));
		while ((line = br.readLine()) != null) {
			BasicMLData data_input = new BasicMLData(inputSize);
			BasicMLData data_ideal = new BasicMLData(outputSize);
			String[] dataRow = line.split(cvsSplitBy);
			for (int i=1; i<dataRow.length;i++) {
				data_input.add(i-1,Double.parseDouble(dataRow[i])); 
			}
			data_ideal.add(Integer.parseInt(dataRow[0]),1);
			trainingSet.add(data_input, data_ideal);
		}
		br.close();
		
       
		BasicNetwork network = new BasicNetwork();
		network.addLayer(new BasicLayer(null, true, inputSize));
		network.addLayer(new BasicLayer(new ActivationTANH(), true, 200));
		network.addLayer(new BasicLayer(new ActivationTANH(), true, outputSize));
		network.getStructure().finalizeStructure();
		network.reset();

		
		// train the neural network
		final Train train = new ResilientPropagation(network, trainingSet);

		int epoch = 1;

		do {
			train.iteration();
			System.out.println("Epoch #" + epoch + " Error:" + train.getError());
			epoch++;
			
			if (train.getError()<old_error) {
				SerializeObject.save(new File("example.net"), network);
			}	

			old_error = train.getError();
		} while (epoch<300);

		// test the neural network
		System.out.println("Neural Network Results:");
	
		
		for (MLDataPair pair : trainingSet) {
			int actual=100;
			int ideal=100;
			double maxOutput=-10000;
			final MLData output = network.compute(pair.getInput());
			
			for (int i=0;i<outputSize;i++) {
				
			    if (output.getData(i) > maxOutput) {
			    	maxOutput = output.getData(i);
			        actual = i;
			    }
			}
			
			for (int i=0;i<output.size();i++) {
			    if ( pair.getIdeal().getData(i)==1) ideal = i;

			}			
			
			System.out.println("actual="+actual + ",ideal=" + ideal);
			totalCount++;
			if (actual==ideal) correctCount++;
		}

		System.out.println("Total = " + totalCount + ", Correct = " + correctCount+", %="+Math.round((((correctCount * 100.0f) / totalCount))*100)/100.0);
		Encog.getInstance().shutdown();
	}
}
