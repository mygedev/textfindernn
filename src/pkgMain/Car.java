package pkgMain;


import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import javax.imageio.ImageIO;

public class Car {
	
	int car_id;
	int imageCount;
	boolean textFound;
	String photo;
	String ip; 
	
	ArrayList<ImageData> carImages = new ArrayList<>();
	
	public Car(int p_carId, int p_imageCount, String p_photo, String p_ip){
		car_id = p_carId;
		imageCount = p_imageCount;
		photo = p_photo;
		textFound = false;
		ip = p_ip;
	}
	
	public void fillCarImages(){
		for (int i=1;i<=imageCount;i++) {
			try {
				carImages.add(new ImageData(getImageUrl(i),1));
			} catch (Exception e) {
				//e.printStackTrace();
			}
		}
		
	}
	
	public String getImageUrl(int imageNumber) {
		return "https://static.my.ge/myauto/photos/large/"+photo+"/"+car_id+"_"+imageNumber+".jpg";
	}

}

