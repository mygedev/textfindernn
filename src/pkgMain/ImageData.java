package pkgMain;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

public class ImageData {
	public String url;
	public double[][] normilizedMatrix;
	public int foundTextCount;
	
	public ImageData(String p_url, int p_url_type){
		url = p_url;
		foundTextCount = 0;
		BufferedImage bImage = null;
		try {
			if (p_url_type==1) bImage = ImageIO.read(new URL(p_url));
			if (p_url_type==2) bImage = ImageIO.read(new File(p_url));
			
			normilizedMatrix = TextFinderNN.generateNormalizedMatrix(bImage);
			
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	

}
