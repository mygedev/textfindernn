package pkgMain;

import java.io.File;
import java.sql.Connection;

import org.ini4j.Wini;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class DB {

	public static Connection connectToDB(String dbName) {
		MysqlDataSource ds;

		try {
			Wini ini = new Wini(new File("settings.ini"));
			
			String db_username = "";
			String db_password = "";
			String db_host = "";
			int db_port = 0;
			String db_database = "";			
			
			if (dbName.equals("myauto")) {
				db_username = ini.get("db", "myauto_username");
				db_password = ini.get("db", "myauto_password");
				db_host = ini.get("db", "myauto_host");
				db_port = ini.get("db", "myauto_port", int.class);
				db_database = ini.get("db", "myauto_db_database");
			}
			
			if (dbName.equals("myusers")) {
				db_username = ini.get("db", "mydb_username");
				db_password = ini.get("db", "mydb_password");
				db_host = ini.get("db", "mydb_host");
				db_port = ini.get("db", "mydb_port", int.class);
				db_database = ini.get("db", "mydb_db_database");
			}
			
			
			ds = new MysqlDataSource();
			ds.setServerName(db_host);
			ds.setPortNumber(db_port);
			ds.setDatabaseName(db_database);
			ds.setUser(db_username);
			ds.setPassword(db_password);			

			return ds.getConnection();

		} catch (Exception e) {
			System.err.println("Exception: " + e.getMessage());
			return null;
		}

	}
}
