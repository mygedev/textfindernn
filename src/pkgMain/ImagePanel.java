package pkgMain;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

class ImagePanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Image img;
	
	Rectangle markArea;
	
	
	public void LoadImage(String fileName) {
		img = new ImageIcon(fileName).getImage();
		setSizes();		
		this.repaint();
	}	

	public void paintComponent(Graphics g) {
		g.drawImage(img, 0, 0, null);
	}	
	
	public void setSizes() {
		Dimension size = new Dimension(img.getWidth(null),img.getHeight(null));
		setPreferredSize(size);
		setMinimumSize(size);
		setMaximumSize(size);
		setSize(size);	
		setLayout(null);		
	}
	
	public void showMark(int x, int y, int width, int height) {
		markArea = new Rectangle(x, y, width, height);
	}
	
	public void hideMark() {
		markArea = null;
	}	
	
	
	public void paint(Graphics g) {
		super.paint(g);
		
		g.setColor(Color.GREEN);
		if (markArea != null) g.drawRect(markArea.x, markArea.y, markArea.width, markArea.height);
		

		
	}	

	
}
