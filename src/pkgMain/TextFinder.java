package pkgMain;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import org.encog.ml.data.MLData;
import org.encog.ml.data.basic.BasicMLData;
import org.encog.neural.networks.BasicNetwork;
import org.encog.util.obj.SerializeObject;

public class TextFinder {

	ImageFrame imageFrame;
	BasicNetwork network;
	double[][] normalizedMatrix;

	int frameWidth = 20;
	int frameHeight = 20;
	BasicMLData input = new BasicMLData(frameWidth * frameHeight);

	public void loadNetwork(String fileName) {
		try {
			network = (BasicNetwork) SerializeObject.load(new File(fileName));
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
	}

	public void loadImage(String fileName) {
		imageFrame = new ImageFrame(fileName);
	}

	public void generateNormilizedMatrix() {
		int red, green, blue;
		int newPixel;
		normalizedMatrix = new double[imageFrame.targetImageBuffer.getWidth()][imageFrame.targetImageBuffer
				.getHeight()];
		double[][] tempMatrix = new double[imageFrame.targetImageBuffer.getWidth()][imageFrame.targetImageBuffer
				.getHeight()];

		for (int j = 0; j < imageFrame.targetImageBuffer.getHeight(); j++) {
			for (int i = 0; i < imageFrame.targetImageBuffer.getWidth(); i++) {
				red = new Color(imageFrame.targetImageBuffer.getRGB(i, j)).getRed();
				green = new Color(imageFrame.targetImageBuffer.getRGB(i, j)).getGreen();
				blue = new Color(imageFrame.targetImageBuffer.getRGB(i, j)).getBlue();

				newPixel = (red + green + blue) / 3;

				tempMatrix[i][j] = newPixel;
			}
		}

		for (int j = 0; j < imageFrame.targetImageBuffer.getHeight(); j++) {
			for (int i = 0; i < imageFrame.targetImageBuffer.getWidth(); i++) {
				normalizedMatrix[i][j] = tempMatrix[i][j] / 255;
			}
		}

	}

	public void copyFrameToInput(int x, int y) {
		input.clear();
		for (int i = 0; i < frameWidth * frameHeight; i++)
			input.add(i, normalizedMatrix[x + (i % frameWidth)][y + (i / frameWidth)]);
	}

	public void showInput() {
		BufferedImage tempImageBuffer = new BufferedImage(frameWidth, frameHeight, BufferedImage.TYPE_INT_RGB);
		int pixel;
		for (int i = 0; i < frameWidth; i++) {
			for (int j = 0; j < frameHeight; j++) {
				pixel = (int) (input.getData(j * frameWidth + i) * 255);
				tempImageBuffer.setRGB(i, j, new Color(pixel, pixel, pixel).getRGB());
			}
		}

		imageFrame.inputImage.img = (Image) tempImageBuffer;
		imageFrame.inputImage.setSizes();
		imageFrame.inputImage.repaint();
	}

	public void markTarget(int x, int y) {

		imageFrame.targetImage.showMark(x, y, frameWidth, frameHeight);
		imageFrame.targetImage.repaint();

	}

	public void markSourcePermanentry(int x, int y) {

		BufferedImage tempImageBuffer = new BufferedImage(imageFrame.targetImageBuffer.getWidth(),
				imageFrame.targetImageBuffer.getHeight(), BufferedImage.TYPE_INT_RGB);
		tempImageBuffer = ImageFrame.toBufferedImage(imageFrame.sourceImage.img);
		// BufferedImage tempImageBuffer = (BufferedImage)
		// imageFrame.sourceImage.img;

		Graphics2D graph = tempImageBuffer.createGraphics();
		graph.setColor(Color.BLACK);
		graph.fillRect(x, y, frameWidth, frameHeight);
		graph.dispose();

		imageFrame.sourceImage.img = (Image) tempImageBuffer;
		imageFrame.sourceImage.repaint();

	}

	public void markText(double threshold) {
		int cnt = 0;
		MLData output;
		for (int x = 0; x < normalizedMatrix.length - frameWidth; x += 10) {
			for (int y = 0; y < normalizedMatrix[0].length - frameHeight; y += 10) {
				copyFrameToInput(x, y);
				output = network.compute(input);
				//System.out.println("x=" + x + ", y=" + y + "; " + String.format("%.5f", output.getData()[0]));
				// showInput();
				if (output.getData()[0] > threshold) {
					markSourcePermanentry(x, y);
					cnt++;
				}

			}
		}
		
		System.out.println("cnt =" +cnt);
	}

}
