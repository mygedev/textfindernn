package pkgMain;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Random;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.encog.app.analyst.AnalystFileFormat;
import org.encog.app.analyst.EncogAnalyst;
import org.encog.app.analyst.csv.normalize.AnalystNormalizeCSV;
import org.encog.app.analyst.script.normalize.AnalystField;
import org.encog.app.analyst.wizard.AnalystWizard;
import org.encog.util.arrayutil.NormalizationAction;
import org.encog.util.csv.CSVFormat;

public class InputGenerator {
	
	public InputGenerator() {
	
	}
	
	public void generateCSVFromImages() {
		
		String dirPrefix = "d:/data";
		
		
		Iterator<?> itPositiveDir = FileUtils.iterateFiles(new File(dirPrefix+"/withText"), null, false);
		Iterator<?> itNegativeDir = FileUtils.iterateFiles(new File(dirPrefix+"/withoutText"), null, false);
		
		Iterator<?>[] inputDirectories = new Iterator<?>[2];
		int[] idealValues = new int[2];
		inputDirectories[0] = itPositiveDir;
		idealValues[0]=1;
		inputDirectories[1] = itNegativeDir;
		idealValues[1]=0;
		int flushCounter = 0;
		
		
		
		String trainFileName = (dirPrefix+"/train.csv");
		String validationFileName = (dirPrefix+"/validation.csv");
		File file;
		int[] divideBy3 = new int[766];
		for (int i = 0; i < divideBy3.length; i++)	divideBy3[i] = (int) (i / 3);
		int red, green, blue;
		int newPixel;
		
		BufferedWriter trainBufferedWriter = null;
		BufferedWriter validationBufferedWriter = null;
		try {
			trainBufferedWriter = new BufferedWriter( new FileWriter(trainFileName));
			validationBufferedWriter = new BufferedWriter( new FileWriter(validationFileName));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		StringBuilder trainStringBuilder = new StringBuilder();
		StringBuilder validationStringBuilder = new StringBuilder();
		StringBuilder currentStringBuilder = null;
		Random randomGenerator = new Random();
		
		for(int k=0;k<inputDirectories.length;k++) {
			while (inputDirectories[k].hasNext()) {
				file = (File) inputDirectories[k].next();
				
				if (randomGenerator.nextInt(9)>0) {
					currentStringBuilder = trainStringBuilder;
					System.out.println(file.getPath()+" train");
				}
				else {
					currentStringBuilder = validationStringBuilder;
					System.out.println(file.getPath()+" validate");
				}
				
				try {
					BufferedImage original = ImageIO.read(file);

					for (int j = 0; j < original.getHeight(); j++) {
						for (int i = 0; i < original.getWidth(); i++) {
						
							// Get pixels by R, G, B
							red = new Color(original.getRGB(i, j)).getRed();
							green = new Color(original.getRGB(i, j)).getGreen();
							blue = new Color(original.getRGB(i, j)).getBlue();
	
							newPixel = divideBy3[red + green + blue];
							currentStringBuilder.append(Integer.toString(255-newPixel) + ",");
						}
					}
					currentStringBuilder.append(idealValues[k]);
					currentStringBuilder.append("\r\n");
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				flushCounter ++;
				if (flushCounter>1000||!inputDirectories[k].hasNext()) {
					try {
						trainBufferedWriter.write(trainStringBuilder.toString());
						trainBufferedWriter.flush();
						validationBufferedWriter.write(validationStringBuilder.toString());
						validationBufferedWriter.flush();
						trainStringBuilder.setLength(0);
						validationStringBuilder.setLength(0);
					} catch (IOException e) {
						e.printStackTrace();
					}
					flushCounter = 0;
				}
	
			}
		}
		
		
		
		try {
			trainBufferedWriter.write(trainStringBuilder.toString());
			trainBufferedWriter.close();
			validationBufferedWriter.write(validationStringBuilder.toString());
			validationBufferedWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
    public void NormalizeData(String sourceFileName, String targetFileName, int inputCount) {
		File sourceFile = new File(sourceFileName);
		File targetFile = new File(targetFileName);
		
		EncogAnalyst analyst = new EncogAnalyst();
		AnalystWizard wizard = new AnalystWizard(analyst);
		wizard.wizard(sourceFile, false, AnalystFileFormat.DECPNT_COMMA);
		
		int i=1;
		for (AnalystField field : analyst.getScript().getNormalize()
				.getNormalizedFields()) {
			if (i++>inputCount) {
				field.setAction(NormalizationAction.PassThrough);
			} else {
				field.setNormalizedHigh(1); 
				field.setNormalizedLow(0);
				field.setAction(NormalizationAction.Normalize);
			}
		}
		dumpFieldInfo(analyst);
		
		final AnalystNormalizeCSV norm = new AnalystNormalizeCSV();
		norm.analyze(sourceFile, false, CSVFormat.ENGLISH, analyst);
		norm.setProduceOutputHeaders(false);
		norm.normalize(targetFile);
	
    	
    }

	public void dumpFieldInfo(EncogAnalyst analyst) {
		System.out.println("Fields found in file:");
		for (AnalystField field : analyst.getScript().getNormalize()
				.getNormalizedFields()) {

			StringBuilder line = new StringBuilder();
			line.append(field.getName());
			line.append(",action=");
			line.append(field.getAction());
			line.append(",min=");
			line.append(field.getActualLow());
			line.append(",max=");
			line.append(field.getActualHigh());
			System.out.println(line.toString());
		}
	}
    
}
