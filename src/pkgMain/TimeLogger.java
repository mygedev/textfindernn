package pkgMain;

public class TimeLogger {
	private long startTime = 0, stopTime = 0;
	String timerName;
	
	public TimeLogger(String p_name) {
		startTimer(p_name);
	}

	public void startTimer(String p_name) {
		startTime = System.currentTimeMillis();
		timerName = p_name;
	}

	public void stopTimer() {
		stopTime = System.currentTimeMillis();
		System.out.println(timerName + " took " + (stopTime - startTime) + " Milliseconds");
	}

}
