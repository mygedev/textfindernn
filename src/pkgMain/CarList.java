package pkgMain;

import java.sql.*;
import java.util.ArrayList;

public class CarList {

	ArrayList<Car> cars = null;
	int imageCountThreshold = 0;
	int maxCarId = 0;

	public CarList(int p_imageCountThreshold) {
		imageCountThreshold = p_imageCountThreshold;
	}

	public void fetchList(int chunkSize) {

		Connection conn = DB.connectToDB("myauto");
		cars = new ArrayList<>();
		int lastCarId = getLastCarId();

		String query = "select c.car_id, c.pic_number, c.photo, cd.client_ip " + "        "
				+ "       from cars c, car_details cd "
				+ "      where c.car_id = cd.car_id "
				+ "        and not exists (SELECT 1 from dealer_users d where d.user_id = c.user_id) "
				+ "        and c.status_id = 1 " + "         and c.car_id > " + lastCarId + " "
				+ "   order by c.car_id " + "       limit " + chunkSize;

		Statement st;
		try {
			st = conn.createStatement();
			ResultSet rs = st.executeQuery(query);

			while (rs.next()) {
				cars.add(new Car(rs.getInt("car_id"), rs.getInt("pic_number"), rs.getString("photo"),
						rs.getString("client_ip")));
				// print the results
				// System.out.format("%s\n", id);
			}
			st.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		for (Car car : cars) {
			car.fillCarImages();
		}

		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public int getLastCarId() {
		Connection conn = DB.connectToDB("myauto");

		int lastCarId = 0;

		String query = "select meta_data_value_int from meta_data where meta_data_id = 1";

		Statement st;
		try {
			st = conn.createStatement();
			ResultSet rs = st.executeQuery(query);

			rs.next();
			lastCarId = rs.getInt("meta_data_value_int");

			st.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		for (Car car : cars) {
			car.fillCarImages();
		}

		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return lastCarId;
	}

	public void updateLastCarId() {
		Connection conn = DB.connectToDB("myauto");

		String query = "update meta_data set meta_data_value_int = " + maxCarId + " where meta_data_id = 1";

		Statement st;
		try {
			st = conn.createStatement();
			st.executeUpdate(query);
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void showCarList() {
		for (Car car : cars) {
			System.out.println(car.car_id + ", " + car.photo + ", " + car.imageCount);
		}
	}

	public void showImageList() {
		for (Car car : cars) {
			for (int i = 1; i <= car.imageCount; i++) {
				System.out.println(car.getImageUrl(i));
			}
		}
	}

	public void findTextInImages(TextFinderNN textFinderNN) {
		int total_cnt = 0;
		int cnt = 0;
		for (Car car : cars) {
			total_cnt = 0;
			if (car.car_id > maxCarId) maxCarId = car.car_id;
			
			
			for (ImageData imageData : car.carImages) {
				if (imageData.normilizedMatrix != null) {
					textFinderNN.normalizedMatrix = imageData.normilizedMatrix;
					cnt = textFinderNN.findText(0.9999);
					imageData.foundTextCount = cnt;
					total_cnt = total_cnt + cnt;
				}
			}
			
			if (total_cnt > imageCountThreshold)	{
				car.textFound = true;
				logSuspect(car);
				System.out.println(car.car_id + ", " + cnt);
			}

		}
	}

	public void logSuspect(Car car) {
		Connection conn = DB.connectToDB("myusers");
		int totalCount = 0;

		for (ImageData imageData : car.carImages) {
			totalCount += imageData.foundTextCount;
		}

		String query = "insert into reported_products (user_id, email, site_id, product_id, report_reason_id, message, ip)"
				+ " values (0, '', 1, " + car.car_id + ", 6, '" + totalCount + "','" + car.ip + "') ";

		Statement st;
		try {
			st = conn.createStatement();
			st.executeUpdate(query);
			st.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
