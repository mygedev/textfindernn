package pkgMain;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.JPanel;


public class ImageFrame extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	ImagePanel sourceImage;
	ImagePanel targetImage;
	ImagePanel inputImage;
	
	BufferedImage targetImageBuffer;
	
	public ImageFrame (String fileName){
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		
		sourceImage = new ImagePanel();
		targetImage = new ImagePanel();
		inputImage = new ImagePanel();
		
		JPanel topPanel = new JPanel(); 
		topPanel.setLayout(new BorderLayout());
		
		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(new FlowLayout(FlowLayout.LEFT,50,50));
		
		
		sourceImage.LoadImage(fileName);

		targetImageBuffer = this.generateBuffer(sourceImage.img);
		targetImage.img = (Image)targetImageBuffer;
		targetImage.setSizes();
		
		bottomPanel.add(inputImage, BorderLayout.EAST);
		

		topPanel.add(sourceImage, BorderLayout.WEST);
		topPanel.add(targetImage, BorderLayout.EAST);
		
		this.add(topPanel, BorderLayout.NORTH);
		this.add(bottomPanel, BorderLayout.SOUTH);

		
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		this.setSize(1500,800);
		
		// Move the window
		int x = (dim.width - this.getWidth()) / 2;
		int y = (dim.height - this.getHeight()) / 2;
		this.setLocation(x, y);
		
	}
	
	
	public BufferedImage generateBuffer(Image img) {
		
		int[] divideBy3 = new int[766];
		for (int i = 0; i < divideBy3.length; i++)	divideBy3[i] = (int) (i / 3);
		int red, green, blue;
		
		int newPixel;
		int invertPixel;
		
		BufferedImage original = toBufferedImage(img);
		
		BufferedImage targetBI= new BufferedImage(original.getWidth(),original.getHeight(),BufferedImage.TYPE_INT_RGB);

		//////////	 delete
		///StringBuilder testStringBuilder = new StringBuilder();;
		///BufferedWriter testBufferedWriter = null ;
		///try {
	    ///testBufferedWriter = new BufferedWriter( new FileWriter("data_test/frame.csv"));
		///} catch (IOException e) {
		///	e.printStackTrace();
		///}
		//////////	 delete
		
		for (int j = 0; j < original.getHeight(); j++) {
			for (int i = 0; i < original.getWidth(); i++) {
			
				// Get pixels by R, G, B
				red = new Color(original.getRGB(i, j)).getRed();
				green = new Color(original.getRGB(i, j)).getGreen();
				blue = new Color(original.getRGB(i, j)).getBlue();

				newPixel = divideBy3[red + green + blue];
				invertPixel = 255-newPixel;
				targetBI.setRGB(i, j, new Color(invertPixel,invertPixel,invertPixel).getRGB());
				//////////	 delete
				///testStringBuilder.append(Integer.toString(invertPixel) + ",");
				//////////	 delete
			}
		}
		
		//////////	 delete
		///try {
		///	testBufferedWriter.write(testStringBuilder.toString());
		///	testBufferedWriter.flush();
		///} catch (IOException e) {
		///	e.printStackTrace();
		///}
		//////////	 delete		
		

		
		return targetBI;
	}
	
	public static BufferedImage toBufferedImage(Image img)
	{
	    if (img instanceof BufferedImage)
	    {
	        return (BufferedImage) img;
	    }

	    // Create a buffered image with transparency
	    BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

	    // Draw the image on to the buffered image
	    Graphics2D bGr = bimage.createGraphics();
	    bGr.drawImage(img, 0, 0, null);
	    bGr.dispose();

	    // Return the buffered image
	    return bimage;
	}
	

}
